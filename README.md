## Getting Started

First, run the development server:

```bash
npm install
# and then
npm run dev
```

## The Issue

Once the application is running, please navigate to the following URL:

URL: http://localhost:3000/jlcomponent

If you then interact with the two elements, you will eventually see the following issue to get displayed.

![Image demonstrating Max Depth Issue](issue.png?raw=true "Max Depth Issue")

The two elements within the above pages are from JL Design System:

- "@JohnLewisPartnership/jl-design-system/dist/elements/input/Input";
- "@JohnLewisPartnership/jl-design-system/dist/elements/select/Select";

However if you then navigate to:

URL: http://localhost:3000/vanilla

You will notice that the same interaction with the components on this page do not cause Max Depth Issue. This is because the elements on this pages are pure html `select` and `input`. Therefore we believe there is some issue within the Design System components that causes this issue.
