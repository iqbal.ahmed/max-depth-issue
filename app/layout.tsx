import "@JohnLewisPartnership/jl-design-system/dist/style/theme/_default.scss";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.scss";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Experimentation Calculator",
  description:
    "The JohnLewis Experimentation Team's Calculator helps plan, execute, and analyze experiments, providing key parameters and efficient result analysis for simple tests or complex studies.",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en" id="page-root">
      <head>
        <link rel="icon" href="/favicon.ico" sizes="any" />
      </head>
      <body className={inter.className}>{children}</body>
    </html>
  );
}
