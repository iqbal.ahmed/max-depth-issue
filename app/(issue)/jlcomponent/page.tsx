"use client";

import Input from "@JohnLewisPartnership/jl-design-system/dist/elements/input/Input";
import Select from "@JohnLewisPartnership/jl-design-system/dist/elements/select/Select";
import { SetStateAction, useState } from "react";
import styles from "./page.module.scss";

export const metricTypes = [
  {
    label: "Boolean",
    value: "Boolean",
  },
  {
    label: "Continuous",
    value: "Continuous",
  },
];

export default function JLComponent() {
  const [goalMetricType, setGoalMetricType] = useState("");
  const [baselineSuccessRate, setBaselineSuccessRate] = useState("");

  return (
    <section className={styles.section}>
      <label className={styles.label} htmlFor="significance">
        Goal metric type
      </label>
      <Select
        data-test="inputs:metric:type"
        className={styles.input}
        ariaLabel="Goal metric type"
        id="inputsMetricTypes"
        name="goalMetricType"
        onChange={(e: { target: { value: SetStateAction<string> } }) =>
          setGoalMetricType(e.target.value)
        }
        value={goalMetricType}
        options={metricTypes}
      />

      <label className={styles.label} htmlFor="baselineSuccessRate">
        Current goal metric value
      </label>
      <Input
        data-test="inputs:current:goal:metric"
        className={styles.input}
        ariaLabel="input-Baseline success rate"
        id="inputsCurrentGoalMetric"
        name="baselineSuccessRate"
        value={baselineSuccessRate}
        onChange={(e) => setBaselineSuccessRate(e.target.value)}
        type="number"
        placeholder="%"
        required={true}
        maxLength="2"
      />
    </section>
  );
}
