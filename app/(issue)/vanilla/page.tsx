"use client";

import { useState } from "react";
import styles from "./page.module.scss";

export const metricTypes = [
  {
    label: "Boolean",
    value: "Boolean",
  },
  {
    label: "Continuous",
    value: "Continuous",
  },
];

export default function Vanilla() {
  const [goalMetricType, setGoalMetricType] = useState("");
  const [baselineSuccessRate, setBaselineSuccessRate] = useState("");

  return (
    <section className={styles.section}>
      <label className={styles.label} htmlFor="significance">
        Goal metric type
      </label>
      <select
        className={styles.input}
        name="goalMetricType"
        id="goalMetricType"
        onChange={(e) => setGoalMetricType(e.target.value)}
        value={goalMetricType}
      >
        <option value="Boolean">Boolean</option>
        <option value="Continuous">Continuous</option>
      </select>

      <label className={styles.label} htmlFor="baselineSuccessRate">
        Current goal metric value
      </label>
      <input
        className={styles.input}
        name="baselineSuccessRate"
        value={baselineSuccessRate}
        type="text"
        id="fname"
        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
          setBaselineSuccessRate(e.target.value)
        }
      ></input>
    </section>
  );
}
